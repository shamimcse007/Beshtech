<?php include_once ('dashboard_head.php');?>
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Extensions</span> - Image Cropper</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="extension_image_cropper.html">Extensions</a></li>
							<li class="active">Image cropper</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Cropper demonstration -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Cropper demonstration</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">This example demonstrates some basic functionality with previews in different sizes. <code>X</code> and <code>Y</code> values display current cropping zone position, <code>width</code> and <code>height</code> values display current cropping zone size. You can get image info and image data by clicking on the proper button on the right side. Also you can change cropping zone aspect ratio and change image source on the fly, try to change image name in <code>1.png</code> - <code>10.png</code> range. 3 buttons below the main image allow you to: reset the cropping zone to the start state, release the cropping zone and reset the cropping zone with new data.</p>

							<div class="row">
								<div class="col-md-6">
									<div class="image-cropper-container content-group" style="height: 400px;">
										<img src="assets/images/placeholder.jpg" alt="" class="cropper">
									</div>
									<div class="row">
										<div class="col-lg-4"><p><button id="reset" type="button" class="btn btn-info btn-block">Reset</button></p></div>
										<div class="col-lg-4"><p><button id="release" type="button" class="btn btn-info btn-block">Release</button></p></div>
										<div class="col-lg-4"><p><button id="setData" type="button" class="btn btn-info btn-block">Set Data</button></p></div>
									</div>
								</div>

								<div class="col-md-6">
									<div class="content-group text-center">
										<div class="eg-preview">
											<div class="preview preview-lg"></div>
											<div class="preview preview-md"></div>
											<div class="preview preview-sm"></div>
											<div class="preview preview-xs"></div>
										</div>
									</div>

									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label class="text-semibold control-label">X value:</label>
												<input type="text" class="form-control" id="dataX" placeholder="x">
											</div>

											<div class="form-group">
												<label class="text-semibold control-label">Width:</label>
												<input type="text" class="form-control" id="dataW" placeholder="width">
											</div>

											<div class="form-group">
												<label class="text-semibold control-label">Get cropping data:</label>
												<div class="input-group">
													<input class="form-control" id="showData" type="text" value="data...">
													<span class="input-group-btn">
														<button class="btn btn-primary" id="getData" type="button">Get Data</button>
													</span>
												</div>
											</div>

											<div class="form-group">
												<label class="text-semibold control-label">Change aspect ratio:</label>
												<div class="input-group">
													<input class="form-control" id="aspectRatio" name="aspectRatio" type="text" value="auto">
													<span class="input-group-btn">
														<button class="btn btn-danger" id="setAspectRatio" type="button">Change</button>
													</span>
												</div>
											</div>
										</div>


										<div class="col-lg-6">
											<div class="form-group">
												<label class="text-semibold control-label">Y value:</label>
												<input type="text" class="form-control" id="dataY" placeholder="y">
											</div>

											<div class="form-group">
												<label class="text-semibold control-label">Height:</label>
												<input type="text" class="form-control" id="dataH" placeholder="height">
											</div>

											<div class="form-group">
												<label class="text-semibold control-label">Get image info:</label>
												<div class="input-group">
													<input class="form-control" id="showInfo" type="text" value="Info...">
													<span class="input-group-btn">
														<button class="btn btn-primary" id="getImgInfo" type="button">Get Info</button>
													</span>
												</div>
											</div>

											<div class="form-group">
												<label class="text-semibold control-label">Change img source:</label>
												<div class="input-group">
													<input class="form-control" id="imgSrc" type="text" value="assets/images/placeholder.jpg">
													<span class="input-group-btn">
														<button class="btn btn-danger" id="setImgSrc" type="button">Change</button>
													</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>


						</div>
					</div>
					<!-- /cropper demonstration -->


					<!-- Basic usage -->
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Basic usage</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">This is the most basic example of <code>cropper</code> implementation. By default cropping area is centered and takes 80% of image space. The image is resizable, but should be wrapped in block element with fixed height.</p>
									<div class="image-cropper-container"><img src="assets/images/placeholder.jpg" alt="" class="crop-basic"></div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Hidden overlay</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">This example demonstrates default options, but with hidden black modal layer above the cropper. To hide the modal set <code>modal</code> option to <code>false</code>. If visible, modal color can be easily changed in css.</p>
									<div class="image-cropper-container"><img src="assets/images/placeholder.jpg" alt="" class="crop-modal"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /basic usage -->


					<!-- Zone options -->
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Fixed position</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">This example demonstrates cropped area that has <code>fixed position</code> and can't be moved. Although it isn't movable, other options remain available: resize, dragging, callbacks, aspect ratios, modal etc.</p>
									<div class="image-cropper-container">
										<img src="assets/images/placeholder.jpg" alt="" class="crop-not-movable">
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Fixed size</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">This example demonstrates cropped area that has <code>fixed size</code> and can't be resized. Although it isn't resizable, other options remain available: moving, dragging, callbacks, aspect ratios, modal etc.</p>
									<div class="image-cropper-container">
										<img src="assets/images/placeholder.jpg" alt="" class="crop-not-resizable">
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /zone options -->


					<!-- Disable functionality -->
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Disabled autocrop</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">In this example cropping zone is not rendered automatically when initialize and available only on a new drag. By default, cropping zone is always <code>visible</code> and can be hidden by setting <code>autoCrop</code> option to <code>false</code>.</p>
									<div class="image-cropper-container"><img src="assets/images/placeholder.jpg" alt="" class="crop-auto"></div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Disabled drag</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">In this example the user can't remove the current cropping zone and create a new one by dragging over the image. By default, this feature is enabled and can be disabled by setting <code>dragCrop</code> option to <code>false</code>.</p>
									<div class="image-cropper-container"><img src="assets/images/placeholder.jpg" alt="" class="crop-drag"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /disable functionality -->


					<!-- Ratios -->
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Fixed 16:9 ratio</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">Thix example demonstrates fixed <code>16:9</code> dragging (selection) ratio. By default, aspect ratio isn't specified and is free. Optional aspect ratios are also available and can be specified using <code>aspectRatio</code> option.</p>
									<div class="image-cropper-container"><img src="assets/images/placeholder.jpg" alt="" class="crop-16-9"></div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Fixed 4:3 ratio</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">Thix example demonstrates fixed <code>4:3</code> dragging (selection) ratio. By default, aspect ratio isn't specified and is free. Optional aspect ratios are also available and can be specified using <code>aspectRatio</code> option.</p>
									<div class="image-cropper-container"><img src="assets/images/placeholder.jpg" alt="" class="crop-4-3"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /ratios -->


					<!-- Zone sizing -->
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Minimum zone size</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">This example demonstrates the <code>minimum</code> width and height (px of original image) of the cropping zone. Better use this option only when you are sure that the image has this <code>minimum</code> width and height. By default, both values aren't specified.</p>
									<div class="image-cropper-container"><img src="assets/images/placeholder.jpg" alt="" class="crop-min"></div>
								</div>
							</div>
						</div>

						<div class="col-md-6">
							<div class="panel panel-flat">
								<div class="panel-heading">
									<h5 class="panel-title">Maximum zone size</h5>
									<div class="heading-elements">
										<ul class="icons-list">
					                		<li><a data-action="collapse"></a></li>
					                		<li><a data-action="reload"></a></li>
					                		<li><a data-action="close"></a></li>
					                	</ul>
				                	</div>
								</div>

								<div class="panel-body">
									<p class="content-group">This example demonstrates the <code>maximum</code> width and height (px of original image) of the cropping zone. Better use this option only when you are sure that the image has this <code>maximum</code> width and height. By default, both values aren't specified.</p>
									<div class="image-cropper-container"><img src="assets/images/placeholder.jpg" alt="" class="crop-max"></div>
								</div>
							</div>
						</div>
					</div>
					<!-- /zone sizing -->

<?php include_once ('dashboard_footer.php');?>