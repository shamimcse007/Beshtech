<?php include_once ('dashboard_head.php');?>
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Uploaders</span> - Plupload</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="uploader_plupload.php">Uploaders</a></li>
							<li class="active">Plupload</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- All runtimes -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">All runtimes</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Plupload is a cross-browser <code>multi-runtime</code> file uploading API. Basically, a set of tools that will help you to build a reliable and visually appealing file uploader in minutes. This example replaced a specific div with a the jQuery uploader <code>queue</code> widget. It will automatically check for different runtimes in the configured order, if it fails it will not convert the specified element.</p>

							<p class="text-semibold">Queue widget example:</p>
							<div class="file-uploader"><p>Your browser doesn't have Flash installed.</p></div>
						</div>
					</div>
					<!-- /all runtimes -->


					<!-- Flash runtime -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Flash runtime</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of multiple file uploader with <code>flash</code> runtime. Flash runtime executes and displays content from a provided <code>SWF</code> file, although it has no in-built features to modify the <code>SWF</code> file at runtime. It can execute software written in the ActionScript programming language which enables the runtime manipulation of text, data, vector graphics, raster graphics, sound and video.</p>

							<p class="text-semibold">Flash runtime example:</p>
							<div class="flash-uploader"><p>Your browser doesn't have Flash installed.</p></div>
						</div>
					</div>
					<!-- /flash runtime -->


					<!-- HTML4 runtime -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">HTML4 runtime</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of multiple fime uploader with <code>HTML4</code> runtime. This kind of runtime doesn't support <code>drag and drop</code> functionality. This is the most standard runtime which supports older versions of all modern browsers. Currently, Plupload may be considered as consisting of three parts: low-level <code>pollyfills</code>, <code>Plupload API</code> and <code>Widgets</code>.</p>

							<p class="text-semibold">HTML4 runtime example:</p>
							<div class="html4-uploader"><p>Your browser doesn't have HTML 4 support.</p></div>
						</div>
					</div>
					<!-- /HTML4 runtime -->


					<!-- HTML5 runtime -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">HTML5 runtime</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of <code>HTML5</code> runtime. The main benefit of HTML5 runtime is that files not only can be picked from browse dialog, but also can be dropped directly from the desktop. In some browsers, mostly in those based on WebKit, it is possible to drag and drop whole folders. Please note:  drag and drop feature will not work in some legacy browsers.</p>

							<p class="text-semibold">HTML5 runtime example:</p>
							<div class="html5-uploader"><p>Your browser doesn't support native upload.</p></div>
						</div>
					</div>
					<!-- /HTML5 runtime -->


					<!-- Events -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Uploader events</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">This example shows you how to bind various <code>events</code> to your plupload instance. Current example demonstrates 2 <code>preInit</code> events: <code>Init</code> - fires when the current RunTime has been initialized and <code>UploadFile</code> - fires when a file is to be uploaded by the runtime. All 15 <code>post init</code> events attached to the uploader you can see in the block below in upload process.</p>

							<p class="text-semibold">Uploader events example:</p>
							<div class="uploader-events content-group"><p>Your browser doesn't support native upload.</p></div>

							<p class="text-semibold">Uploader events:</p>
							<div class="well well-white pre-scrollable" id="log"></div>
						</div>
					</div>
					<!-- /events -->


<?php include_once ('dashboard_footer.php');?>