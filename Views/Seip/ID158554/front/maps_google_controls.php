
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Google Maps</span> - Controls</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="maps_google_controls.php">Google maps</a></li>
							<li class="active">Controls</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Disabled UI -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Disable default UI</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of a map with <code>disabled</code> default UI. The maps displayed through the Google Maps API contain UI elements to allow user interaction with the map. These elements are known as controls and you can include variations of these controls in your Google Maps API application. Alternatively, you can do nothing and let the Google Maps API handle all control behavior. By default maps UI is enabled.</p>

							<div class="map-container map-ui-disabled"></div>
						</div>
					</div>
					<!-- /disabled UI -->


					<!-- Adding controls -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Adding controls</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">You may wish to tailor your interface by <code>removing</code>, <code>adding</code>, or <code>modifying</code> UI behavior or controls. If you wish to only add or modify existing behavior, you need to ensure that the control is explicitly added to your application. Some controls appear on the map by default while others will not appear unless you specifically request them. Adding or removing controls from the map is specified in the <code>MapOptions</code> object's fields.</p>
							
							<div class="map-container map-adding-controls"></div>
						</div>
					</div>
					<!-- /adding controls -->


					<!-- Control options -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Control options</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Several controls are configurable, allowing you to <code>alter</code> their behavior or <code>change</code> their appearance. The Zoom control, for example, may display as either a large control with a full zoom control with slider, or as a smaller, mini-zoom control for smaller maps. These controls are modified by altering appropriate control options fields within the <code>MapOptions</code> object upon creation of the map.</p>

							<div class="map-container map-control-options"></div>
						</div>
					</div>
					<!-- /control options -->


					<!-- Control positioning -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Control positioning</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Most of control options contain a <code>position</code> property which indicates where on the map to place the control. Positioning of these controls is not absolute; instead, the API will layout the controls intelligently by "flowing" them around existing map elements, or other controls, within given constraints (such as the map size). <strong>Note</strong>: all positions may coincide with positions of UI elements whose placements you may not modify.</p>

							<div class="map-container map-control-positioning"></div>
						</div>
					</div>
					<!-- /control positioning -->

<?php include_once ('dashboard_footer.php')?>