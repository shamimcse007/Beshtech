<?php include_once ('dashboard_head.php')?>

	<!-- Page container -->


			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Editors</span> - CKEditor</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="index.html"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="editors_ckeditor.html">Editors</a></li>
							<li class="active">CKEditor</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- CKEditor default -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Full featured CKEditor</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">CKEditor is a ready-for-use HTML text editor designed to simplify web content creation. It's a WYSIWYG editor that brings common word processor features directly to your web pages. It benefits from an active community that is constantly evolving the application with free add-ons and a transparent development process.</p>
							<form action="#">
								<div class="content-group">
									<textarea name="editor-full" id="editor-full" rows="4" cols="4">
										<h2>Apollo 11</h2>
										<div class="pull-right" style="margin-left: 20px;"><img alt="Saturn V carrying Apollo 11" class="right" src="http://c.cksource.com/a/1/img/sample.jpg"></div>

										<p><strong>Apollo 11</strong> was the spaceflight that landed the first humans, Americans <a href="#">Neil Armstrong</a> and <a href="#">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.</p>

										<p class="mb-15">Armstrong spent about <s>three and a half</s> two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third member of the mission, <a href="#">Michael Collins</a>, piloted the <a href="#">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.</p>

										<h5 class="text-semibold">Technical details</h5>
										<p>Launched by a <strong>Saturn V</strong> rocket from <a href="#">Kennedy Space Center</a> in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of <a href="#">NASA</a>'s Apollo program. The Apollo spacecraft had three parts:</p>
										<ol>
											<li><strong>Command Module</strong> with a cabin for the three astronauts which was the only part which landed back on Earth</li>
											<li><strong>Service Module</strong> which supported the Command Module with propulsion, electrical power, oxygen and water</li>
											<li><strong>Lunar Module</strong> for landing on the Moon.</li>
										</ol>
										<p class="mb-15">After being sent to the Moon by the Saturn V's upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the <a href="#">Sea of Tranquility</a>. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the <a href="#">Pacific Ocean</a> on July 24.</p>

										<h5 class="text-semibold">Misson crew</h5>

										<table class="table table-bordered" style="width: 100%">
											<thead>
												<tr>
													<th>Position</th>
													<th>Astronaut</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Commander</td>
													<td>Neil A. Armstrong</td>
												</tr>
												<tr>
													<td>Command Module Pilot</td>
													<td>Michael Collins</td>
												</tr>
												<tr>
													<td>Lunar Module Pilot</td>
													<td>Edwin "Buzz" E. Aldrin, Jr.</td>
												</tr>
											</tbody>
										</table>

										Source: <a href="http://en.wikipedia.org/wiki/Apollo_11">Wikipedia.org</a>
						            </textarea>
					            </div>

					            <div class="text-right">
						            <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-right14 position-right"></i></button>
					            </div>
				            </form>
						</div>
					</div>
					<!-- /CKEditor default -->


					<!-- CKEditor readonly API -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">CKEditor readonly API</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="mb-15">This sample shows how to use the <code>setReadOnly</code> API to put editor into the read-only state that makes it impossible for users to change the editor contents. In readonly mode all toolbar buttons are disabled, except <code>copy</code>, <code>full screen</code> and <code>source</code> buttons, they are still accessible. Content is selectable, but not editable.</p>

							<p class="content-group">
					            <button type="button" class="btn btn-primary" id="readOnlyOn" style="display: none;"><i class="icon-eye2 position-left"></i> Make editor readonly</button>
					            <button type="button" class="btn btn-success" id="readOnlyOff" style="display: none;"><i class="icon-eye-blocked2 position-left"></i> Make it editable</button>
				            </p>

							<form action="#">
								<div class="content-group">
									<textarea class="ckeditor" name="editor-readonly" id="editor-readonly" rows="4" cols="4">
										<h2>Apollo 11</h2>
										<div class="pull-right" style="margin-left: 20px;"><img alt="Saturn V carrying Apollo 11" class="right" src="http://c.cksource.com/a/1/img/sample.jpg"></div>

										<p><strong>Apollo 11</strong> was the spaceflight that landed the first humans, Americans <a href="#">Neil Armstrong</a> and <a href="#">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.</p>

										<p class="mb-15">Armstrong spent about <s>three and a half</s> two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third member of the mission, <a href="#">Michael Collins</a>, piloted the <a href="#">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.</p>

										<h5 class="text-semibold">Technical details</h5>
										<p>Launched by a <strong>Saturn V</strong> rocket from <a href="#">Kennedy Space Center</a> in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of <a href="#">NASA</a>'s Apollo program. The Apollo spacecraft had three parts:</p>
										<ol>
											<li><strong>Command Module</strong> with a cabin for the three astronauts which was the only part which landed back on Earth</li>
											<li><strong>Service Module</strong> which supported the Command Module with propulsion, electrical power, oxygen and water</li>
											<li><strong>Lunar Module</strong> for landing on the Moon.</li>
										</ol>
										<p class="mb-15">After being sent to the Moon by the Saturn V's upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the <a href="#">Sea of Tranquility</a>. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the <a href="#">Pacific Ocean</a> on July 24.</p>

										<h5 class="text-semibold">Misson crew</h5>

										<table class="table table-bordered" style="width: 100%">
											<thead>
												<tr>
													<th>Position</th>
													<th>Astronaut</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Commander</td>
													<td>Neil A. Armstrong</td>
												</tr>
												<tr>
													<td>Command Module Pilot</td>
													<td>Michael Collins</td>
												</tr>
												<tr>
													<td>Lunar Module Pilot</td>
													<td>Edwin "Buzz" E. Aldrin, Jr.</td>
												</tr>
											</tbody>
										</table>

										Source: <a href="http://en.wikipedia.org/wiki/Apollo_11">Wikipedia.org</a>
						            </textarea>
					            </div>

					            <div class="text-right">
						            <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-right14 position-right"></i></button>
					            </div>
				            </form>
						</div>
					</div>
					<!-- /CKEditor readonly API -->


					<!-- Enter key configuration -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">ENTER Key Configuration</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">This sample shows how to configure the <code>Enter</code> and <code>Shift+Enter</code> keys to perform actions specified in the enterMode and <code>shiftEnterMode</code> parameters, respectively. You can choose from the following options: <span class="text-semibold">ENTER_P</span> – new <code>&lt;p></code> paragraphs are created; <span class="text-semibold">ENTER_BR</span> – lines are broken with <code>&lt;br></code> elements; <span class="text-semibold">ENTER_DIV</span> – new <code>&lt;div></code> blocks are created.</p>

							<form action="#">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="text-semibold">When <kbd>Enter</kbd> is pressed:</label>
											<select class="select" id="xEnter">
												<option selected="selected" value="1">Create a new &lt;P&gt; (recommended)</option>
												<option value="3">Create a new &lt;DIV&gt;</option>
												<option value="2">Break the line with a &lt;BR&gt;</option>
											</select>
										</div>
									</div>

									<div class="col-md-6">
										<div class="form-group">
											<label class="text-semibold">When <kbd>Shift+Enter</kbd> is pressed:</label>
											<select class="select" id="xShiftEnter">
												<option value="1">Create a new &lt;P&gt;</option>
												<option value="3">Create a new &lt;DIV&gt;</option>
												<option selected="selected" value="2">Break the line with a &lt;BR&gt; (recommended)</option>
											</select>
										</div>
									</div>
								</div>

								<div class="mb-15">
									<textarea name="editor-enter" id="editor-enter" rows="4" cols="4">
										<h2>Apollo 11</h2>
										<div class="pull-right" style="margin-left: 20px;"><img alt="Saturn V carrying Apollo 11" class="right" src="http://c.cksource.com/a/1/img/sample.jpg"></div>

										<p><strong>Apollo 11</strong> was the spaceflight that landed the first humans, Americans <a href="#">Neil Armstrong</a> and <a href="#">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.</p>

										<p class="mb-15">Armstrong spent about <s>three and a half</s> two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third member of the mission, <a href="#">Michael Collins</a>, piloted the <a href="#">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.</p>

										<h5 class="text-semibold">Technical details</h5>
										<p>Launched by a <strong>Saturn V</strong> rocket from <a href="#">Kennedy Space Center</a> in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of <a href="#">NASA</a>'s Apollo program. The Apollo spacecraft had three parts:</p>
										<ol>
											<li><strong>Command Module</strong> with a cabin for the three astronauts which was the only part which landed back on Earth</li>
											<li><strong>Service Module</strong> which supported the Command Module with propulsion, electrical power, oxygen and water</li>
											<li><strong>Lunar Module</strong> for landing on the Moon.</li>
										</ol>
										<p class="mb-15">After being sent to the Moon by the Saturn V's upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the <a href="#">Sea of Tranquility</a>. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the <a href="#">Pacific Ocean</a> on July 24.</p>

										<h5 class="text-semibold">Misson crew</h5>

										<table class="table table-bordered" style="width: 100%">
											<thead>
												<tr>
													<th>Position</th>
													<th>Astronaut</th>
												</tr>
											</thead>
											<tbody>
												<tr>
													<td>Commander</td>
													<td>Neil A. Armstrong</td>
												</tr>
												<tr>
													<td>Command Module Pilot</td>
													<td>Michael Collins</td>
												</tr>
												<tr>
													<td>Lunar Module Pilot</td>
													<td>Edwin "Buzz" E. Aldrin, Jr.</td>
												</tr>
											</tbody>
										</table>

										Source: <a href="http://en.wikipedia.org/wiki/Apollo_11">Wikipedia.org</a>
						            </textarea>
					            </div>

					            <div class="text-right">
						            <button type="submit" class="btn bg-teal-400">Submit form <i class="icon-arrow-right14 position-right"></i></button>
					            </div>
				            </form>
						</div>
					</div>
					<!-- /enter key configuration -->


					<!-- CKEditor inline -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Inline editor</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">This sample shows how to create an inline editor instance of CKEditor. You can also create an inline editor from a textarea element. In this case the <code>textarea</code> will be replaced by a <code>div</code> element with inline editing enabled. To enter edit mode, click on any element inside div or textarea elements, toolbar will be sticked to the top of this element.</p>

							<hr>

							<div id="editor-inline" contenteditable="true">
								<h1>Apollo 11</h1>
								<img alt="Saturn V carrying Apollo 11" class="pull-right" style="margin-left: 20px;" src="http://c.cksource.com/a/1/img/sample.jpg">

								<p><strong>Apollo 11</strong> was the spaceflight that landed the first humans, Americans <a href="#">Neil Armstrong</a> and <a href="#">Buzz Aldrin</a>, on the Moon on July 20, 1969, at 20:18 UTC. Armstrong became the first to step onto the lunar surface 6 hours later on July 21 at 02:56 UTC.</p>

								<p class="mb-15">Armstrong spent about <s>three and a half</s> two and a half hours outside the spacecraft, Aldrin slightly less; and together they collected 47.5 pounds (21.5&nbsp;kg) of lunar material for return to Earth. A third member of the mission, <a href="#">Michael Collins</a>, piloted the <a href="#">command</a> spacecraft alone in lunar orbit until Armstrong and Aldrin returned to it for the trip back to Earth.</p>

								<h5 class="text-semibold">Technical details</h5>
								<p>Launched by a <strong>Saturn V</strong> rocket from <a href="#">Kennedy Space Center</a> in Merritt Island, Florida on July 16, Apollo 11 was the fifth manned mission of <a href="#">NASA</a>'s Apollo program. The Apollo spacecraft had three parts:</p>
								<ol>
									<li><strong>Command Module</strong> with a cabin for the three astronauts which was the only part which landed back on Earth</li>
									<li><strong>Service Module</strong> which supported the Command Module with propulsion, electrical power, oxygen and water</li>
									<li><strong>Lunar Module</strong> for landing on the Moon.</li>
								</ol>
								<p class="mb-15">After being sent to the Moon by the Saturn V's upper stage, the astronauts separated the spacecraft from it and travelled for three days until they entered into lunar orbit. Armstrong and Aldrin then moved into the Lunar Module and landed in the <a href="#">Sea of Tranquility</a>. They stayed a total of about 21 and a half hours on the lunar surface. After lifting off in the upper part of the Lunar Module and rejoining Collins in the Command Module, they returned to Earth and landed in the <a href="#">Pacific Ocean</a> on July 24.</p>

								<h5 class="text-semibold">Misson crew</h5>

								<table class="table table-bordered content-group" style="width: 100%">
									<thead>
										<tr>
											<th>Position</th>
											<th>Astronaut</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>Commander</td>
											<td>Neil A. Armstrong</td>
										</tr>
										<tr>
											<td>Command Module Pilot</td>
											<td>Michael Collins</td>
										</tr>
										<tr>
											<td>Lunar Module Pilot</td>
											<td>Edwin "Buzz" E. Aldrin, Jr.</td>
										</tr>
									</tbody>
								</table>

								Source: <a href="http://en.wikipedia.org/wiki/Apollo_11">Wikipedia.org</a>
							</div>
						</div>
					</div>
					<!-- /CKEditor inline -->


				<?php include_once ('dashboard_footer.php')?>