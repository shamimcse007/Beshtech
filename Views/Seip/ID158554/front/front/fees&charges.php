<?php
require_once "head.php";
?>
<h4 style="background-color: #2b542c;color:white;">FEES AND CHARGES FOR PAYMENTS VIA PAYPAL, SKRILL, NETELLER, WEBMONEY, PERFECTMONEY FROM BANGLADESH</h4>
    <div class="content">
    <div class="container-fluid">
        <div style="width: 300px;  border:1px double darkgreen; float: right;">
        <img src="../../../../img/advartising.jpg">
        </div>
        <p class="text-success" style="font-size: 18px">
            All fees & charges are changeable. PaymentBD has the right to increase / decrease fees any time.<br><br>
            1 USD = 80.00 Taka (For PayPal Payments)<br>
            1 USD = 90.00 Taka (For Skrill / Moneybookers Payments)<br>
            1 USD = 90.00 Taka (For NETELLER balance)<br>
            1 USD = 87.00 Taka (For PerfectMoney / WebMoney balance)<br>
            1 USD = 83.00 Taka (For Payza Payments)<br>
            1 USD = 90.00 Taka (For Credit Card Payments)<br><br>
            1 USD = 78.42 Taka (For Receive via our PayPal / Payza)<br>
            1 USD = 80.00 Taka (For Receive via our Skrill / Moneybookers / WebMoney / PerfectMoney)<br><br>
            Special USD Rate: 85 Taka (When You Sell NETELLER to us)<br>
            So, if you open a Business account at our website, you will get Net Amount 1 USD = 80.75 Taka (Per Dollar)<br>
            So, if you open a Premier account at our website, you will get Net Amount 1 USD = 78.20 Taka (Per Dollar)<br>
            So, if you open a Personal account at our website, you will get Net Amount 1 USD = 76.50 Taka (Per Dollar)<br><br>
            For iTunes Gift Cards: Special Flat Rate $1 USD = 100 Taka (No extra service charge). But client must be an active member.<br><br>
            $10 USD Card = 1,000 Taka<br>
            $25 USD Card = 2,500 Taka<br>
            $50 USD Card = 5,000 Taka<br>
            $100 USD Card = 10,000 Taka<br><br>
            Click Here To Calculate Cost To Make Any Payment<br><br>
            Personal Account Fees:<br>
            Yearly Subscription Fee: 200 Taka<br>
            Pay via PayPal / Credit Card: 10% (Min: 300 BDT)<br>
            Receive from Freelancing Sites: 10% (Min: 5 BDT)<br>
            Withdraw fund: 00 Taka (You will collect cheque from office)<br>
            Withdraw fund: 30 Taka (We deposit cheque at your Bank)<br><br>
            Business Account Fees:<br>
            Yearly Subscription Fee: 1000 Taka<br>
            Pay via PayPal / Credit Card: 5% (Min: 300 BDT)<br>
            Receive from Freelancing Sites: 5% (Min: 5 BDT)<br>
            Withdraw fund: 00 Taka (You will collect cheque from office)<br>
            Withdraw fund: 30 Taka (We deposit cheque at your Bank)<br><br>
            Premier Account Fees:<br>
            Yearly Subscription Fee: 500 Taka<br>
            Pay via PayPal / Credit Card: 8% (Min: 300 BDT)<br>
            Receive from Freelancing Sites: 8% (Min: 5 BDT)<br>
            Withdraw fund: 00 Taka (You will collect cheque from office)<br>
            Withdraw fund: 30 Taka (We deposit cheque at your Bank)<br><br>
            Click Here To Calculate Cost To Make Any Payment<br><br>
            Domain Name Registration Price List<br>
            Buy any .com domain @ 900 Taka<br>
            Buy any .net domain @ 900 Taka<br>
            Buy any .org domain @ 900 Taka<br>
            Buy any .biz domain @ 900 Taka<br>
            Buy any .info domain @ 900 Taka<br><br>
            Domain + Web Hosting Price List<br>
            1 Domain + 100 MB Web Hosting => 1,500 Taka<br>
            1 Domain + 200 MB Web Hosting => 2,000 Taka<br>
            1 Domain + 500 MB Web Hosting => 2,500 Taka<br>
            1 Domain + 1 GB Web Hosting => 3,000 Taka<br>
            1 Domain + Unlimited Web Hosting => 6,500 Taka<br>
            Web Hosting Price List (Without Domain)<br>
            100 MB Web Hosting => 1,000 Taka<br>
            200 MB Web Hosting => 1,500 Taka<br>
            500 MB Web Hosting => 2,000 Taka<br>
            1 GB Web Hosting => 2,500 Taka<br>
            Unlimited Web Hosting => 6,000 Taka<br>
            <strong> Office Location:</strong> 51/1B Bosilla city-Mohammadpur -Dhaka-1207<br><br>
        <div class="videoembade01">
            <h3 style="color :green; padding:5px;"> Pls wathch this video for know about us :</h3>
            <br><br>
            <iframe width="900" height="415"
                    src="https://www.youtube.com/embed/XGSy3_Czz8k">
            </iframe>
            <h3>Here are some pictures of PaymentBD.com Dhaka Office and our team members</h3>
        </div>
        </p>
    </div>
</div>
    </div>
<?php
require_once "footer.php";
?>