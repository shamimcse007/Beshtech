<?php
require_once "head.php";
?>
<div class="content">
    <div class="wrapper">
        <div class="container-fluid">
            <div class="signup">
                <div class="col-sm-4"  id="signuppersonal" style="width: 370px" >
                    <h5 style="color :green; padding:5px; background-color:lightseagreen; border-radius:10px;"> DOMAIN NAME + HOSTING</h5><br>
                    <p class="text-info">
                    Domain + Web Hosting Price List<br>
                    1 Domain + 100 MB Web Hosting => 1,500 Taka<br>
                    1 Domain + 200 MB Web Hosting => 2,000 Taka<br>
                    1 Domain + 500 MB Web Hosting => 2,500 Taka<br>
                    1 Domain + 1 GB Web Hosting => 3,000 Taka<br>
                    1 Domain + Unlimited Web Hosting=>6,500 Taka<br><br>
                        Web Hosting Price List (Without Domain)<br>
                    100 MB Web Hosting => 1,000 Taka<br>
                    200 MB Web Hosting => 1,500 Taka<br>
                    500 MB Web Hosting => 2,000 Taka<br>
                    1 GB Web Hosting => 2,500 Taka<br>
                    Unlimited Web Hosting => 6,000 Taka<br>
                    <p style="font-size: 20px; color:darkslategray;">You can choose any .com, .org, .net, .biz, .info domain.</p><br>
                    <div class="container" style="border:1px double darkgreen;width: 320px">
                        <p style="color:green;background-color: #9d9d9d"; >Search for your Domain Here</p>
                        <form action="" method="post" class="form-group">
                            <label>www: &nbsp;</label><input type="search" name="term" /><label>&nbsp;Search</label><br />
                        </form>
                        <form>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.com
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.net
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.org
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.info
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.biz
                            </label>
                        </form>
                        <input type="submit" name="submit" value="Submit" class="btn btn-success" style="width:200px;" />
                    </div>
                </div>
                <div class="col-sm-4"  id="signuppremium" style="width: 360px">
                    <h5 style="color :green; padding:5px; background-color:lightseagreen; border-radius:10px;"> WEB HOSTING (WITHOUT DOMAIN)</h5><br>
                    <p class="text-info">
                    Web Hosting Price List (Without Domain)<br>
                    100 MB Web Hosting => 1,000 Taka<br>
                    200 MB Web Hosting => 1,500 Taka<br>
                    500 MB Web Hosting => 2,000 Taka<br>
                    1 GB Web Hosting => 2,500 Taka<br>
                    Unlimited Web Hosting => 6,000 Taka<br><br>
                        Domain + Web Hosting Price List<br>
                    1 Domain + 100 MB Web Hosting => 1,500 Taka<br>
                    1 Domain + 200 MB Web Hosting => 2,000 Taka<br>
                    1 Domain + 500 MB Web Hosting => 2,500 Taka<br>
                    1 Domain + 1 GB Web Hosting => 3,000 Taka<br>
                    1 Domain + Unlimited Web Hosting=>6,500 Taka<br>
                    <p style="font-size: 20px; color:darkslategray;">You can choose any .com, .org, .net, .biz, .info domain.</p><br>
                    <div class="container" style="border:1px double darkgreen;width: 320px">
                        <p style="color:green;background-color: #9d9d9d"; >Search for your Domain Here</p>
                        <form action="" method="post" class="form-group">
                            <label>www: &nbsp;</label><input type="search" name="term" /><label>&nbsp;Search</label><br />
                        </form>
                        <form>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.com
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.net
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.org
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.info
                            </label>
                            <label class="checkbox-inline">
                                <input type="checkbox" value="">.biz
                            </label>
                        </form>
                        <input type="submit" name="submit" value="Submit" class="btn btn-success" style="width:200px;" />
                    </div>
                </div>
                <div  class="col-sm-4"  id="signupbusiness" style="width: 370px" >
                    <h5 style="color :green; padding:5px; background-color:lightseagreen; border-radius:10px;"> BUSINESS ACCOUNT</h5>
                    <p class="text-info">
                    Buy Dedicated Server: Intel Core2Duo 1.6GHz @ $50 USD Per Month<br>
                    Buy Dedicated Server: i5-750 @ $100 USD Per Month<br>
                    Buy Dedicated Server: Opteron 2210 @ $100 USD Per Month<br>
                    Buy Dedicated Server: Dual Xeon 5150 4x 250GB @ $100 USD Per Month<br>
                    Buy Dedicated Server: Xeon 5520 Preconfig 4x250GB @ $150 USD Per Month<br>
                    Buy Dedicated Server: Dual Opteron 2373 @ $175 USD Per Month<br>
                    Buy Dedicated Server: Xeon 5520 @ $175 USD Per Month<br>
                    </p>
                    <p style="font-size: 20px; color:darkslategray;">You can choose any .com, .org, .net, .biz, .info domain.</p><br>
                    <div class="container" style="border:1px double darkgreen;width: 320px">
                    <p style="color:green;background-color: #9d9d9d"; >Search for your Domain Here</p>
                    <form action="" method="post" class="form-group">
                        <label>www: &nbsp;</label><input type="search" name="term" /><label>&nbsp;Search</label><br />
                    </form>
                    <form>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="">.com
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="">.net
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="">.org
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="">.info
                        </label>
                        <label class="checkbox-inline">
                            <input type="checkbox" value="">.biz
                        </label><br><br>
                    </form>
                    <input type="submit" name="submit" value="Submit" class="btn btn-success" style="width:200px;" />
                </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
require_once "footer.php";
?>