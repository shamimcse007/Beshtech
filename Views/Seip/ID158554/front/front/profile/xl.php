<?php
error_reporting(E_ALL);
error_reporting(E_ALL & ~E_DEPRECATED);
ini_set('display_errors', TRUE);
ini_set('display_stratup_errors',TRUE);
date_default_timezone_set('Europe/London');


if(PHP_SAPI =='cli')
    die('This example should only run from a Web Browser');
require_once'../../../../vendor/phpoffice/phpexcel/Classes/PHPExcel.php';
include_once ('../../../../vendor/autoload.php');
use App\Seip\Id158554\Mobile\Mobile;
$obj= new Mobile();
$allData =$obj->index();
//create new PHPExcel object
$objPHPExcel = new PHPExcel();


//set document properties

$objPHPExcel->getProperties()->setCreator('shamim')
    ->setLastModifiedBy('shamim')
    ->setTitle('final project')
    ->setSubject('Seip documentations')
    ->setDescription('we shall over come')
    ->setKeywords("core code")
    ->setCategory('we are learning');
// add some data
$objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A1','SL')
    ->setCellValue('B1','ID')
    ->setCellValue('C1','mobile_model')
    ->setCellValue('d1','mobile_name')
    ->setCellValue('e1','mobile_price');
$counter=2;
$serial=0;



foreach ($allData as $data)
{
    $serial++;
    $objPHPExcel->setActiveSheetIndex(0)
    ->setCellValue('A'.$counter, $serial)
    ->setCellValue('B'.$counter, $data['id'])
    ->setCellValue('c'.$counter, $data['mobile_model'])
    ->setCellValue('d'.$counter, $data['mobile_name'])
    ->setCellValue('e'.$counter, $data['mobile_price']);
    $counter++;
}
//Rename Vorksheet
$objPHPExcel->getActiveSheet()->setTitle('mobile_list');

//set active sheet index to the first sheet, so Excel opens the
$objPHPExcel->setActiveSheetIndex(0);

//Redirect output to a client's veb browser (Excel5)
header('Content-Type: application/vnd.ms-excel');
header('Content-Disposition: attachment; filename="01simple.xls"');
header('Cache-control: max-age-1');

header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
header('Lst-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
header('Cache-Control: cache,must-revlidate');
header('Pragma:Public');

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
$objWriter->save('php://output');
exit;
?>