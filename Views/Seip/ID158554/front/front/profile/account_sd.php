<?php
include_once ('../../../../../vendor/autoload.php');
use App\Seip\Id158554\User\User;
$obj= new User();
$obj->setData($_GET);
$value = $obj->show();
//echo "<pre>";
//print_r($value);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>final project</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>
<body style="">

<table class="table table-bordered" style="width: 600px;margin:0 auto; position:relative;  ">
    <h3> <button style="margin: 0 auto"><a href="#">Full Profile</a> </button></h3>
    <h3> <button style="margin: 0 auto"><a href="../dashboard.php">Return to Dashboard</a> </button></h3>
    <thead>
    <tr>
        <th>NID</th>
        <th>Name</th>
        <th>Mobile</th>
        <th>Account Type</th>
        <th>Email</th>
        <th>Account Status</th>
        <th>Action</th>


    </tr>
    </thead>
    <tbody>
    <tr>
            <td> <?php echo $value['n_id'] ?></td>
            <td><?php echo $value['name']?></td>
            <td><?php echo $value['mobile']?></td>
            <td><?php echo $value['account_type']?></td>
            <td><?php echo $value['email']?></td>
            <td>Pending</td>
            <td>
                <a href="update.php ?id=<?php echo $value['id']?>">Edit</a> |&nbsp;&nbsp;
                <a href="trash.php ?id=<?php echo $value['id']?>">Trash</a>
            </td>
    </tr>

    </tbody>
</table>

</body>
</html>
