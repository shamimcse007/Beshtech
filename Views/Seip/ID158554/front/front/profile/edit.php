<?php
include_once ('../../../../../vendor/autoload.php');
use App\Seip\Id158554\User\User;
$obj= new User();
$obj->setData($_GET);
$value = $obj-> show();
//echo "<pre>";
//print_r($value);
?>
<div id="register" class="animate form">
    <form action="store.php" METHOD="post">
        <h1> EDIT PROFILE</h1>
        <p>
            <label for="n_id" class="uname" data-icon="u"> National ID </label>
            <input id="n_id" name="n_id" required="required" type="text" placeholder=" National ID"/>
        </p>
        <p>
            <label for="name" class="uname" data-icon="u">Name</label>
            <input id="name" name="name" required="required" type="text" placeholder=" name"/>
        </p>
        <p>
            <label for="mobile" class="uname" data-icon="u">Mobile</label>
            <input id="mobile" name="mobile" required="required" type="text" placeholder=" Mobile"/>
        </p>
        <p>
            <label for="account_type" class="uname" >Account Type</label>
            <select  id="account_type" name="account_type"  required="required" >
                <option value="Personal">Personal</option>
                <option value="Premium">Premium</option>
                <option value="Business">Business</option>
            </select>
        </p>

        <p>
            <label for="emailsignup" class="youmail" data-icon="e"> Your email</label>
            <input id="emailsignup" name="email" required="required" type="email" placeholder="mysupermail@mail.com"/>
        </p>

        <p class="signin button">
            <input type="submit" value="Update"/>
        </p>


    </form>
</div>

