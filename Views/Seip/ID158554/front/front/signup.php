<?php
include_once("../../../../vendor/autoload.php");
require_once "head.php";

?>
<!--                stat this page   -->
<div class="content">
    <div class="wrapper">
        <div class="container-fluid">
            <div class="signup">
                        <div class="col-sm-4"  id="signuppersonal">
                            <p class="text-content-font">
                            <h3 style="color :green; padding:5px; background-color:lightseagreen; border-radius:10px;"> PERSONAL ACCOUNT</h3><br>
                            Very High Transaction Fee: 10%<br>

                            Sign Up for Personal Account<br>

                            Fees & Charges<br>
                            Subscription Fee: 200 Tk/Year<br>
                            Pay via PayPal: 10% (Min: 300 Tk)
                            Receive Freelancing Money: 10% (Min 5 Tk)<br>
                            Pay Online (BD Sites): 5% (Min 5 Tk)<br>
                            Receive Money(BD Sites): 1% (Min 5 Tk)<br>
                            1 USD = 80.00 Taka (For PayPal Payments)<br>
                            1 USD = 83.00 Taka (Skrill, WebMoney)
                            1 USD = 83.00 Taka (Neteller, Perfect Money)<br>
                            1 USD = 78.42 Taka (Receive via PayPal)<br>
                            Calculate Cost To Make Any Payment<br> <br><br>
                            <button type="button" class="btn btn-success"><a href="registration.php">Sign Up Now</a></button>
                                </p>
                        </div>
                        <div class="col-sm-4"  id="signuppremium">
                            <p class="text-content-font">
                            <h3 style="color :green; padding:5px; background-color:lightseagreen; border-radius:10px;"> PREMIER ACCOUNT</h3><br>
                                High Transaction Fee: 8%<br>

                                Sign Up for Premier Account<br>

                                Fees & Charges<br>
                                Subscription Fee: 500 Tk/Year<br>
                                Pay via PayPal: 8% (Min: 300 Tk)<br>
                                Receive Freelancing Money: 8% (Min 5 Tk)<br>
                                Pay Online (BD Sites): 4% (Min 5 Tk)<br>
                                Receive Money (BD Sites): 1% (Min 5 Tk)<br>
                                1 USD = 80.00 Taka (For PayPal Payments)<br>
                                1 USD = 83.00 Taka (Skrill, WebMoney)<br>
                                1 USD = 83.00 Taka (Neteller, Perfect Money)<br>
                                1 USD = 78.42 Taka (Receive via PayPal)<br>
                                Calculate Cost To Make Any Payment<br> <br><br>
                            <button type="button" class="btn btn-success"><a href="registration.php">Sign Up Now</a></button>
                            </p>
                        </div>
                        <div  class="col-sm-4"  id="signupbusiness">
                        <p class="text-content-font">
                            <h3 style="color :green; padding:5px; background-color:lightseagreen; border-radius:10px;"> BUSINESS ACCOUNT</h3>
                            Low Transaction Fee: 5%<br>
                            Sign Up for Business Account<br>
                            Fees & Charges<br>
                            Subscription Fee: 1000 Tk/Year<br>
                            Pay via PayPal: 5% (Min: 300 Tk)<br>
                            Receive Freelancing Money: 5% (Min 5 Tk)<br>
                            Pay Online (BD Sites): BUSINESS_ACCOUNT_SENDING_CHARGE%<br>
                            Receive Money (BD Sites): 1%<br>
                            1 USD = 80.00 Taka (For PayPal Payments)<br>
                            1 USD = 83.00 Taka (Skrill, WebMoney)<br>
                            1 USD = 83.00 Taka (Neteller, Perfect Money)<br>
                            1 USD = 78.42 Taka (Receive via PayPal)<br>
                            Calculate Cost To Make Any Payment<br> <br><br>
                            <button type="button" class="btn btn-success"><a href="registration.php">Sign Up Now</a></button>
                        </p>
                        </div>

                    </div>
        </div>
    </div>
</div>

<?php
require_once "footer.php";
?>