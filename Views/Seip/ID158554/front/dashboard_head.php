
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>themelock.com - Limitless - Responsive Web Application Kit by Eugene Kopyov</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/core.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/components.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/minified/colors.min.css" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="assets/js/plugins/loaders/pace.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/jquery.min.js"></script>
    <script type="text/javascript" src="assets/js/core/libraries/bootstrap.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/loaders/blockui.min.js"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/visualization/d3/d3_tooltip.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/switchery.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/forms/selects/bootstrap_multiselect.js"></script>
    <script type="text/javascript" src="assets/js/plugins/ui/moment/moment.min.js"></script>
    <script type="text/javascript" src="assets/js/plugins/pickers/daterangepicker.js"></script>

    <script type="text/javascript" src="assets/js/core/app.js"></script>
    <script type="text/javascript" src="assets/js/pages/dashboard.js"></script>
    <!-- /theme JS files -->

</head>

<body>

<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-header">
<!--        logo-->
        <a class="navbar-brand" href="index.php"><img src="assets/images/logo_light.png" alt=""></a>

        <ul class="nav navbar-nav visible-xs-block">
            <li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
            <li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
        </ul>
    </div>

    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>

                <div class="dropdown-menu dropdown-content">
                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>

        </ul>

        <p class="navbar-text"><span class="label bg-success-400">Online</span></p>

        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown language-switch">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/images/flags/gb.png" class="position-left" alt="">
                    English
                    <span class="caret"></span>
                </a>

                <ul class="dropdown-menu">
                    <li><a class="deutsch"><img src="assets/images/flags/de.png" alt=""> Deutsch</a></li>
                    <li><a class="ukrainian"><img src="assets/images/flags/ua.png" alt=""> Українська</a></li>
                    <li><a class="english"><img src="assets/images/flags/gb.png" alt=""> English</a></li>
                    <li><a class="espana"><img src="assets/images/flags/es.png" alt=""> España</a></li>
                    <li><a class="russian"><img src="assets/images/flags/ru.png" alt=""> Русский</a></li>
                </ul>
            </li>

            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-bubbles4"></i>
                    <span class="visible-xs-inline-block position-right">Messages</span>
                    <span class="badge bg-warning-400">2</span>
                </a>

                <div class="dropdown-menu dropdown-content width-350">
                    <div class="dropdown-content-heading">
                        Messages
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-compose"></i></a></li>
                        </ul>
                    </div>

                    <ul class="media-list dropdown-content-body">
                        <li class="media">
                            <div class="media-left">

                                <img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                                <span class="badge bg-danger-400 media-badge">5</span>
                            </div>
<!--messages-->
                            <div class="media-body">
                                <a href="#" class="media-heading">
                                    <span class="text-semibold">Shamim</span>
                                    <span class="media-annotation pull-right">04:58</span>
                                </a>

                                <span class="text-muted">who knows, maybe that would be the best thing for me...</span>
                            </div>
                        </li>

                        <li class="media">
                            <div class="media-left">
                                <img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt="">
                                <span class="badge bg-danger-400 media-badge">4</span>
                            </div>

                            <div class="media-body">
                                <a href="#" class="media-heading">
                                    <span class="text-semibold">Hossain</span>
                                    <span class="media-annotation pull-right">12:16</span>
                                </a>

                                <span class="text-muted">That was something he was unable to do because...</span>
                            </div>
                        </li>
<!--/messages-->
                        <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All messages"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>
            </li>

            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
<!--    profile picture  -->
                    <img src="assets/images/placeholder.jpg" alt="">
                    <span>Shamim Hossain</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                    <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
                    <li><a href="#"><span class="badge bg-teal-400 pull-right">58</span> <i class="icon-comment-discussion"></i> Messages</a></li>
                    <li class="divider"></li>
                    <li><a href="#"><i class="icon-cog5"></i> Account settings</a></li>
                    <li><a href="#"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                            <div class="media-body">
                                <span class="media-heading text-semibold">Shamim</span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> Mohammadpur - Dhaka-1227
                                </div>
                            </div>
                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /user menu -->
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <!-- Main -->
                            <li class="navigation-header"><span>Main</span> <i class="icon-menu" title="Main pages"></i></li>
                            <li class="active"><a href="dashboard.php"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                            <li>

                            <li>
                                <a href="#"><i class="icon-footprint"></i> <span>Wizards</span></a>
                                <ul>
                                    <li><a href="wizard_steps.php">Steps wizard</a></li>
                                    <li><a href="wizard_form.php">Form wizard</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="icon-spell-check"></i> <span>Editors</span></a>
                                <ul>
                                    <li><a href="editor_summernote.php">Summernote editor</a></li>
                                    <li><a href="editor_ckeditor.php">CKEditor</a></li>
                                    <li><a href="editor_code.php">Code editor</a></li>
                                </ul>
                            </li>

                            <!-- /forms -->

                            <!-- Appearance -->

                            <!-- /appearance -->

                            <!-- Layout -->


                            <!-- /layout -->

                            <!-- Data visualization -->





                            <li>
                                <a href="#"><i class="icon-map5"></i> <span>Maps integration</span></a>
                                <ul>
                                    <li>
                                        <a href="#">Google maps</a>
                                        <ul>
                                            <li><a href="maps_google_basic.php">Basics</a></li>
                                            <li><a href="maps_google_controls.php">Controls</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="maps_vector.php">Vector maps</a></li>
                                </ul>
                            </li>
                            <!-- /data visualization -->

                            <!-- Extensions -->


                            <li>
                                <a href="uploader_plupload.php"><i class="icon-upload"></i> <span>File uploaders</span></a>
                                <ul>
                                    <li><a href="uploader_plupload.php">Plupload</a></li>
                                    <li><a href="uploader_bootstrap.php">Bootstrap file uploader</a></li>
                                    <li><a href="uploader_dropzone.php">Dropzone</a></li>
                                </ul>
                            </li>
                            <li><a href="extension_image_cropper.php"><i class="icon-crop2"></i> <span>Image cropper</span></a></li>
                            <li>
                                <a href="extension_fullcalendar_advanced.php"><i class="icon-calendar3"></i> <span>Event calendars</span></a>

                            </li>

                            <!-- /extensions -->

                            <!-- Tables -->


                            <!-- /tables -->

                            <!-- Page kits -->

                            <li>
                                <a href="#"><i class="icon-people"></i> <span>User pages</span></a>
                                <ul>
                                    <li><a href="user_pages_cards.php">User cards</a></li>
                                    <li><a href="user_pages_list.php">User list</a></li>
                                    <li><a href="user_pages_profile.php">Simple profile</a></li>
                                    <li><a href="user_pages_profile_cover.php">Profile with cover</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="#"><i class="icon-user-plus"></i> <span>Login &amp; registration</span></a>
                                <ul>
                                    <li><a href="login.php">More login info</a></li>
                                    <li><a href="registration.php">More registration info</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="timelines_left.php"><i class="icon-magazine"></i> <span>Timelines</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="icon-lifebuoy"></i> <span>Support</span></a>
                                <ul>
                                    <li><a href="support_conversation_layouts.html">Conversation layouts</a></li>
                                    <li><a href="support_conversation_options.html">Conversation options</a></li>
                                    <li><a href="support_knowledgebase.html">Knowledgebase</a></li>
                                    <li><a href="support_faq.html">FAQ page</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="search_basic.php"><i class="icon-search4"></i> <span>Search</span></a>
                            </li>
                            <li>
                                <a href="#"><i class="icon-images2"></i> <span>Gallery</span></a>
                                <ul>
                                    <li><a href="gallery_grid.html">Media grid</a></li>
                                    <li><a href="gallery_titles.html">Media with titles</a></li>
                                    <li><a href="gallery_description.html">Media with description</a></li>
                                    <li><a href="gallery_library.html">Media library</a></li>
                                </ul>
                            </li>

                            <!-- /page kits -->

                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->
