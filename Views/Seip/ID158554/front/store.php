<?php
include_once("../../../../vendor/autoload.php");
use App\Seip\ID158554\User\User;
$obj = new User();
if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    if ($_POST['password'] == $_POST['confirm_password']) {
        $available = $obj->setData($_POST)->validity();
        if (empty($available)) {
            $obj->setData($_POST)->store();
        } else {
            $_SESSION['fail'] = "Username or Email already exists";
            header('location:registration.php');
        }

    } else {
        $_SESSION['fail'] = "Password and confirm password does not match";
        header('location:registration.php');
    }

} else {
    $_SESSION['fail'] = "You're not authorized to access this page";
    header('location:registration.php');
}