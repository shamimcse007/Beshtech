<?php include_once ('dashboard_head.php')?>

			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Maps</span> - Vector</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="maps_vector.php">Maps</a></li>
							<li class="active">Vector</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- World map -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">World vector map</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of a <code>world map</code> in <code>SVG</code> format with <code>zoom</code> feature. The map automatically scales on small window sizes. Vector maps work in all modern browsers, including Internet Explorer since it uses native browser technologies like JavaScript, CSS, HTML, SVG or VML. No Flash or any other proprietary browser plug-in is required. Also this allows vector maps to work in all modern mobile browsers.</p>

							<div class="map-container map-world"></div>
						</div>
					</div>
					<!-- /world map -->


					<!-- Choropleth map -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Choropleth map</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of a <code>choropleth</code> world map. A choropleth map is a thematic map in which areas are shaded or patterned in proportion to the measurement of the statistical variable being displayed on the map. The choropleth map provides an easy way to visualize how a measurement varies across a geographic area or it shows the level of variability within a region. This example displays GDP by country, data stored in <code>json</code> file.</p>
							
							<div class="map-container map-choropleth"></div>
						</div>
					</div>
					<!-- /choropleth map -->


					<!-- Custom markers -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Custom markers</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Simple example of a basic world map with optional 1 size <code>markers</code>. Set of markers are added to the map during initialization. In case of array is provided, codes of markers will be set as string representations of array indexes. Each marker is represented by <code>latLng</code> (array of two numeric values), <code>name</code> (string which will be show on marker's label) and any marker <code>styles</code>. Marker size, colors, strokes, opacity can be changed in map settings. </p>

							<div class="map-container map-world-markers"></div>
						</div>
					</div>
					<!-- /custom markers -->


					<!-- Regions selection -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Regions selection</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example map of <code>Germany</code> with selectable <code>regions</code> and <code>markers</code>. The following example demonstrates the feature of vector maps that allows <code>selection</code> of regions and/or markers by user or programmatically. Specific styles could be assigned for the selected items. User selection is saved between page reloads in the local storage by using <code>onRegionSelected</code> and <code>onMarkerSelected</code> functions. The circle size depends on the data values.</p>

							<div class="map-container map-regions"></div>
						</div>
					</div>
					<!-- /regions selection -->


					<!-- Country choropleth map -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Country choropleth map</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of <code>USA</code> choropleth map with markers. The following example demonstrates the visualization of unemployment statistics in the USA by states and metropolitan areas in 2009. Demo data is stored in <code>JSON</code> file. Tooltips with 2 different types of data: regions display state name and unemployment rate, custom markers display full info available in JSON file: population, name and unemployment rate.</p>

							<div class="map-container map-unemployment"></div>
						</div>
					</div>
					<!-- /country choropleth map -->

<?php include_once('dashboard_footer.php'); ?>