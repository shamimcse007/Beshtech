
<?php include_once ('dashboard_head.php');?>

			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Google Maps</span> - Basic</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="maps_google_basic.php">Google maps</a></li>
							<li class="active">Basic</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Basic map -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Basic map</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of simple <code>google map</code> with default options and center in Budapest, Hungary. Zoom and center of the map is defined in chart settings. Google API loads using default synchronous method when the page renders after the script is loaded. Asynchronous method is optional and available for usage. Google Maps uses a close variant of the <code>Mercator projection</code>, and therefore cannot accurately show areas around the poles.</p>

							<div class="map-container map-basic"></div>
						</div>
					</div>
					<!-- /basic map -->


					<!-- Geolocation -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">HTML5 geolocation</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">The HTML5 <code>geolocation</code> returns a location and accuracy radius based on information about cell towers and WiFi nodes that the mobile client can detect. This document describes the protocol used to send this data to the server and to return a response to the client. This example requires that you consent to location sharing when prompted by your browser. If you see a blank space instead of the map, this is probably because you have denied permission for location sharing.</p>

							<div class="map-container map-geolocation"></div>
						</div>
					</div>
					<!-- /geolocation -->


					<!-- Coordinates -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Showing coordinates</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of showing <code>pixel</code> and <code>tile</code> coordinates. The following example demonstrates latitude and longitude, world coordinates, pixel coordinates, tile coordinates and zoom level for Chicago, displaying all this data in a simple info window. Try to zoom in/zoom out the map to see how pixel and tile coordinates change according to the center position. Zoom value also updates according to the current level.</p>

							<div class="map-container map-coordinates"></div>
						</div>
					</div>
					<!-- /coordinates -->


					<!-- Click event -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Click event</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Example of a simple map <code>click</code> event. By default, the input map is centered and zoomed to the bounding box of the contents of the layer. Clicking on the marker sets the zoom level of the map, zooming in increases the zoom level. On map drag there is a 3 seconds timeout after the center of the map has changed, the marker is paned back and map is centered.</p>

							<div class="map-container map-click-event"></div>
						</div>
					</div>
					<!-- /click event -->

<?php include_once ('dashboard_footer.php')?>