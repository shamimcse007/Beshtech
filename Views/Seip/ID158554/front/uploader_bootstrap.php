<?php include_once('dashboard_head.php') ?>
			<!-- Main content -->
			<div class="content-wrapper">

				<!-- Page header -->
				<div class="page-header">
					<div class="page-header-content">
						<div class="page-title">
							<h4><i class="icon-arrow-left52 position-left"></i> <span class="text-semibold">Uploaders</span> - BS File Inputs</h4>
						</div>

						<div class="heading-elements">
							<div class="heading-btn-group">
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-bars-alt text-primary"></i><span>Statistics</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calculator text-primary"></i> <span>Invoices</span></a>
								<a href="#" class="btn btn-link btn-float has-text"><i class="icon-calendar5 text-primary"></i> <span>Schedule</span></a>
							</div>
						</div>
					</div>

					<div class="breadcrumb-line">
						<ul class="breadcrumb">
							<li><a href="dashboard.php"><i class="icon-home2 position-left"></i> Home</a></li>
							<li><a href="uploader_bootstrap.php">Uploaders</a></li>
							<li class="active">BS file inputs</li>
						</ul>

						<ul class="breadcrumb-elements">
							<li><a href="#"><i class="icon-comment-discussion position-left"></i> Support</a></li>
							<li class="dropdown">
								<a href="#" class="dropdown-toggle" data-toggle="dropdown">
									<i class="icon-gear position-left"></i>
									Settings
									<span class="caret"></span>
								</a>

								<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="#"><i class="icon-user-lock"></i> Account security</a></li>
									<li><a href="#"><i class="icon-statistics"></i> Analytics</a></li>
									<li><a href="#"><i class="icon-accessibility"></i> Accessibility</a></li>
									<li class="divider"></li>
									<li><a href="#"><i class="icon-gear"></i> All settings</a></li>
								</ul>
							</li>
						</ul>
					</div>
				</div>
				<!-- /page header -->


				<!-- Content area -->
				<div class="content">

					<!-- Bootstrap file input -->
					<div class="panel panel-flat">
						<div class="panel-heading">
							<h5 class="panel-title">Bootstrap file input</h5>
							<div class="heading-elements">
								<ul class="icons-list">
			                		<li><a data-action="collapse"></a></li>
			                		<li><a data-action="reload"></a></li>
			                		<li><a data-action="close"></a></li>
			                	</ul>
		                	</div>
						</div>

						<div class="panel-body">
							<p class="content-group">Bootstrap <code>file input</code> plugin enhances the HTML 5 file input for Bootstrap 3.x into an advanced widget with file preview for various files, multiple selection and more. The plugin enhances these concepts and simplifies the widget initialization with simple HTML markup on a file input. It offers support for previewing a wide variety of files i.e. images, text, html, video, audio, flash, and objects.</p>

							<form class="form-horizontal">
								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Single file upload:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input">
										<span class="help-block">Automatically convert a file input to a bootstrap file input widget by setting its class as <code>file-input</code>.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Multiple file upload:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input" multiple="multiple">
										<span class="help-block">Automatically convert a file input to a bootstrap file input widget by setting its class as <code>file-input</code>.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Hidden thumbnails:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input" data-show-preview="false">
										<span class="help-block">Hide file preview thumbnails.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Using data-attributes:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input" multiple="multiple" data-show-upload="false" data-show-caption="true">
										<span class="help-block">Use file input attributes (e.g. multiple upload) for setting input behavior and data attributes to control plugin options. For example, hide/show display of upload button and caption.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Disabled input:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input" disabled="disabled">
										<span class="help-block">Set the file input widget to be <code>readonly</code> or <code>disabled</code>.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Hidden caption:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input" data-show-caption="false" data-show-upload="false">
										<span class="help-block">Hide the caption and display widget with only buttons.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Input group sizing:</label>
									<div class="col-lg-10">
										<div class="form-group">
											<input type="file" class="file-input" data-main-class="input-group-lg">
											<span class="help-block">Large file input</span>
										</div>

										<div class="form-group">
											<input type="file" class="file-input">
											<span class="help-block">Default file input</span>
										</div>

										<div class="form-group">
											<input type="file" class="file-input" data-main-class="input-group-sm">
											<span class="help-block">Small file input</span>
										</div>

										<div class="form-group">
											<input type="file" class="file-input" data-main-class="input-group-xs">
											<span class="help-block">Mini file input</span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Input button sizing:</label>
									<div class="col-lg-10">
										<div class="form-group form-group-lg">
											<input type="file" class="file-input" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-lg btn-icon" data-remove-class="btn btn-danger btn-lg btn-icon">
											<span class="help-inline ml-10">Large file input button</span>
										</div>

										<div class="form-group">
											<input type="file" class="file-input" data-show-caption="false" data-show-upload="false">
											<span class="help-inline ml-10">Default file input button</span>
										</div>

										<div class="form-group">
											<input type="file" class="file-input" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-sm btn-icon" data-remove-class="btn btn-danger btn-sm btn-icon">
											<span class="help-inline ml-10">Small file input button</span>
										</div>

										<div class="form-group">
											<input type="file" class="file-input" data-show-caption="false" data-show-upload="false" data-browse-class="btn btn-primary btn-xs btn-icon" data-remove-class="btn btn-danger btn-xs btn-icon">
											<span class="help-inline ml-10">Mini file input button</span>
										</div>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Overwrite styles:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input-custom" accept="image/*">
										<span class="help-block">Show only image files for selection &amp; preview. Control button labels, styles, and icons for the browse, upload, and remove buttons.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Templates modification:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input-advanced" multiple="multiple">
										<span class="help-block">Advanced customization using templates. For example, change position of buttons from right to left.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Specify file extensions:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input-extensions">
										<span class="help-block">Allow only specific file extensions. In this example only <code>jpg</code>, <code>gif</code>, <code>png</code> and <code>txt</code> extensions are allowed.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Specify file types:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input" accept="image/*, video/*">
										<span class="help-block">Allow only <code>image</code> and <code>video</code> file types to be uploaded. You can configure the condition for validating the file types using <code>`fileTypeSettings`</code>.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Always display preview:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input-preview">
										<span class="help-block">Set maximum file upload size to 100 KB. Display preview on load with preset files/images and captions with <code>overwriteInitial</code> set to <code>false</code>. So the initial preview is always displayed when additional files are overwritten (useful for multiple upload) scenario.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Display preview:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input-overwrite">
										<span class="help-block">Display preview on load with preset files/images and captions with <code>overwriteInitial</code> set to <code>true</code>.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Block button:</label>
									<div class="col-lg-10">
										<input type="file" class="file-input" data-browse-class="btn btn-primary btn-block btn-icon" data-show-remove="false" data-show-caption="false" data-show-upload="false">
										<span class="help-block">Display the widget as a single block button.</span>
									</div>
								</div>

								<div class="form-group">
									<label class="col-lg-2 control-label text-semibold">Using plugin methods:</label>
									<div class="col-lg-10">
										<p><input type="file" class="file-input" id="file-input-methods"></p>
										<button type="button" class="btn btn-success" id="btn-modify">Disable file input</button>
										<span class="help-block">Using plugin methods to alter input at runtime. For example, click the <code>Modify</code> button to disable the plugin and change plugin options..</span>
									</div>
								</div>
							</form>
						</div>
					</div>
					<!-- /bootstrap file input -->


				<?php include_once ('dashboard_footer.php');?>