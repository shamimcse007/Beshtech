<?php
namespace App\Seip\ID158554\User;
use PDO ;
include_once("../../../../vendor/autoload.php");

class User
{
    private $id = '';
    private $title = '';
    private $n_id='';
    private $name='';
    private $mobile='';
    private $account_type='';
    private $username='';
    private $email='';
    private $password;
    private $pdo = '';
    private $keyword = '';
    private $token;

    public function __construct()
    {
        session_start();
        $this->pdo = new PDO('mysql:host=localhost;dbname=beshtech', 'root', '');
    }

    public function setData($data ='')
    {
        if (array_key_exists('id',$data)) {
            $this->id = $data['id'];
        }
        if (array_key_exists('n_id', $data)) {
        $this->n_id = $data['n_id'];
        }
        if (array_key_exists('name', $data)) {
            $this->name = $data['name'];
        }
        if (array_key_exists('mobile', $data)) {
            $this->mobile = $data['mobile'];
        }
        if (array_key_exists('account_type', $data)) {
            $this->account_type = $data['account_type'];
        }

        if (array_key_exists('username', $data)) {
            $this->username = $data['username'];
        }
        if (array_key_exists('email', $data)) {
            $this->email = $data['email'];
        }
        if (array_key_exists('password', $data)) {
            $this->password = $data['password'];
        }
        if (array_key_exists('token', $data)) {
            $this->token = $data['token'];
        }

        return $this;

    }

    public function validity()
    {
        try {
            $query = "SELECT * FROM `users` where email='$this->email' OR username='$this->username'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function emailVerification()
    {
        try {
            $query = "SELECT  * FROM `users` where token='$this->token'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            if (!empty($data)) {
                $query = 'UPDATE users SET is_active = 1 WHERE token = :tk';
                $stmt = $this->pdo->prepare($query);
                $stmt->execute(array(
                    ':tk' => $this->token
                ));
                $_SESSION['message'] = "Email Verified. You can login now";
                header('location:registration.php');

            }

        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }

    public function store()
    {
        $token = sha1($this->username);

        try {
            $query2 = "INSERT INTO `users` (`id`, `unique_id`, `n_id`, `name`, `mobile`, `account_type`, `username`, `email`, `password`, `token`, `is_active`)
 VALUES(:id, :unique_id, :n_id, :name, :mobile, :account_type, :un, :email, :pw, :tk, :is_active)";
            $stmt = $this->pdo->prepare($query2);
            $stmt->execute(
                array(
                    ':id' => null,
                    ':unique_id'=> uniqid(),
                    ':n_id'=>$this->n_id,
                    ':name'=>$this->name,
                    'mobile'=>$this->mobile,
                    ':account_type'=>$this->account_type,
                    ':un' => $this->username,
                    ':email' => $this->email,
                    ':pw' => $this->password,
                    ':tk' => $token,
                    ':is_active' => 0
                )
            );
            $link = "Click the link for verify your email. http://sumonmhd.com/verify.php?token=$token";
            $subject = "Email Verification";
            mail($this->email, $subject, $link);
            if ($stmt) {
                $_SESSION['message'] = "Successfully Registered Verify Your email for login !";
                header('location:registration.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }


    public function login()
    {
        try {
            $query = "SELECT  * FROM `users` where email='$this->email' AND password=$this->password";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();

            if (empty($data)) {
                $_SESSION['fail'] = "Opps! Invalid email or password";
                header('location:login.php');
            } elseif ($data['is_active'] == 0) {
                $_SESSION['fail'] = "Email not verified yet";
                header('location:login.php');
            } else {
                $_SESSION['user_info'] = $data;
                header('location:dashboard.php');
            }


        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }

    }


    public function show()
    {
        try {
            $query = "SELECT * from `users` WHERE unique_id=" . "'" . $this->id . "'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetch();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }


    public function update()
    {
        try {
            $query = 'UPDATE mobiles SET title = :t WHERE id = :id';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(
                ':id' => $this->id,
                ':t' => $this->title,
                ':unique_id'=> uniqid(),
                ':n_id'=>$this->n_id,
                ':name'=>$this->name,
                'mobile'=>$this->mobile,
                ':account_type'=>$this->account_type,
                ':un' => $this->username,
                ':email' => $this->email,
                ':pw' => $this->password,
                ':is_active' => 0,
            ));
            if ($stmt) {
                $_SESSION['message'] = "Successfully Updated";
                header('location:account_sd.php.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trash()
    {
        $uni_id = "'" . $this->id . "'";

        try {
            $query = 'UPDATE mobiles SET deleted_at = :delete_time WHERE unique_id =' . $uni_id;
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(
                ':delete_time' => date('Y-m-d h:m:s'),
                ':'

            ));
            if ($stmt) {
                $_SESSION['message'] = "Successfully Updated";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function trashed()
    {
        try {
            $query = "SELECT * FROM `mobiles` where `deleted_at` !='0000-00-00 00:00:00'";
            $stmt = $this->pdo->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchAll();
            return $data;
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function delete()
    {
        try {
            $query = "DELETE FROM `mobiles` WHERE `mobiles`.`id` =" . $this->id;
            $stmt = $this->pdo->query($query);
            $stmt->execute();
            if ($stmt) {
                $_SESSION['message'] = "Successfully Deleted";
                header('location:trashList.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }

    public function restore()
    {
        try {
            $query = 'UPDATE mobiles SET deleted_at = :delete_time WHERE id = :id';
            $stmt = $this->pdo->prepare($query);
            $stmt->execute(array(
                ':delete_time' => '0000-00-00 00:00:00',
                ':id' => $this->id,
            ));
            if ($stmt) {
                $_SESSION['message'] = "Successfully Restore";
                header('location:index.php');
            }
        } catch (PDOException $e) {
            echo 'Error: ' . $e->getMessage();
        }
    }
}











