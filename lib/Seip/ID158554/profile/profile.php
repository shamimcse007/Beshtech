<?php
namespace App\Seip\Id158554\profile ;
use PDO;
class profile
{
    private $id = '';
    private $title='';
    private $mobile_name_p='';
    private $mobile_price_p='';
    private $keyword='';

    function __construct()
   {
       $this-> pdo = new PDO ('mysql:host=localhost;dbname=finalp','root' ,'' );
       session_start();
    }
    function setData($data='')
    {
        if(array_key_exists('mobile_model',$data))
        {
            $this->title = $data['mobile_model'] ;
        }
       if(array_key_exists('mobile_name', $data)){
           $this->mobile_name_p = $data['mobile_name'] ;
       }
       if(array_key_exists('mobile_price',$data)){
           $this->mobile_price_p = $data['mobile_price'] ;
       }
       if(array_key_exists('id',$data)){
           $this-> id =$data['id'];
       }
        if(array_key_exists('keyword',$data)){
            $this-> keyword =$data['keyword'];
        }
        return $this;
    }
    function  store(){
        try{
    $query= "INSERT INTO mobile_info (`id`,`unic_id`, `mobile_model`, `mobile_name`,`mobile_price`) VALUES (:id, :unic_id, :mobile_model,:mobile_name ,:mobile_price)";
   $test= $this->pdo->prepare($query);
    $test->execute(array
    (
            ':id'=> null,
            ':unic_id'=> uniqid(),
            ':mobile_model' =>$this ->title,
            ':mobile_name' => $this ->mobile_name_p,
            ':mobile_price' => $this ->mobile_price_p
    )
);
    echo $test ->rowCount();
    if($test)
    {
        $_SESSION['message'] = "Successfully Added";
        header('location:create.php');
    }
}catch (PDOException $e)
{
    echo 'Error:' . $e->getMessage();
}
}
public function index($itemPerPage='',$offset=''){
    try{
        $query = "SELECT SQL_CALC_FOUND_ROWS * FROM `mobile_info` WHERE deleted_at = '0000-00-00 00:00:00' ORDER BY id DESC LIMIT $itemPerPage OFFSET $offset";
        $show = $this->pdo->prepare($query);
        $show->execute();
        $data = $show->fetchAll();
        $totalRows=$this->pdo->query('SELECT FOUND_ROWS();')->fetch(PDO::FETCH_COLUMN);
        $data['totalRow']=$totalRows;
        //echo $itm;
        return $data;
    }catch (PDOException $e)
    {
        echo 'Error:' . $e->getMessage();
    }
}

public function searchResult($itemPerPage='',$offset='' ){

    try{
        $query = "SELECT  * FROM `mobile_info` WHERE deleted_at = '0000-00-00 00:00:00' AND mobile_model ="."'".$this->keyword ."'"." ORDER BY id DESC LIMIT $itemPerPage OFFSET $offset";
//        echo $query;
//        die();
        $show = $this->pdo->prepare($query);
        $show->execute();
        $data = $show->fetchAll();
        //echo $itm;
        return $data;
    }catch (PDOException $e)
    {
        echo 'Error:' . $e->getMessage();
    }
}

public function show()
{
    try{
        $query = "SELECT * FROM `mobile_info` WHERE `unic_id` = "."'".$this->id."'";
        $show = $this->pdo->prepare($query);
        $show->execute();
        $value = $show->fetch();

        return $value;

    }catch (PDOException $e)
    {
        echo 'Error:' . $e->getMessage();
    }
}
public function trash()
{
    $uni_id="'".$this->id."'";
    try {
        $query='UPDATE mobile_info SET deleted_at=:deleted_time WHERE unic_id ='.$uni_id;
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(array(
            ':deleted_time' => date('Y-m-d h:m:s'),

         ));
        if($stmt){
            $_SESSION['message']= "Record is trashed";
            header('location:index.php');
        }

    } catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
}
public function trashed()
{
    try{
        $query = "SELECT * FROM `mobile_info` WHERE deleted_at != '0000-00-00 00:00:00'";
        $show = $this->pdo->prepare($query);
        $show->execute();
        $data = $show->fetchAll();
        return $data;
    }catch (PDOException $e)
    {
        echo 'Error:' . $e->getMessage();
    }
}

public function Delete()
{
    try{
        $query =  "DELETE FROM `mobile_info` WHERE `mobile_info`.`unic_id`= "."'".$this->id."'";
        $show = $this->pdo->query($query);
        $show->execute();
        if($show){
            $_SESSION['message']= "Record is deleted";
            header('location:trashlist.php');
        }


    }catch (PDOException $e)
    {
        echo 'Error:' . $e->getMessage();
    }
}

public function restore()
{
    try {
        $query="UPDATE mobile_info SET deleted_at=:deleted_time WHERE `unic_id` = "."'".$this->id."'";
        $stmt = $this->pdo->prepare($query);
        $stmt->execute(array(
            ':deleted_time' => '0000-00-00 00:00:00',
        ));
        if($stmt){
            $_SESSION['message']= "Record is trashed";
            header('location:trashlist.php');
        }

    } catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
}
public function update(){
    $uni_id="'".$this->id."'";
    try {
        $query="UPDATE `mobile_info` SET `mobile_model` = :mobile_model,`mobile_name` =:mobile_name, `mobile_price` =:mobile_price WHERE `unic_id` =. $uni_id";
        $sala = $this->pdo->prepare($query);
        $sala->execute(array(
            ':mobile_model' => $this->title,
            ':mobile_name' => $this->mobile_name_p,
            ':mobile_price'=> $this->mobile_price_p
        ));
        if($sala){
            $_SESSION['message']= "Record is updated";
            header('location:index.php');
        }
    } catch(PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
}
}